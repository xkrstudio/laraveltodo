@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <!-- Display Validation Errors -->
            @include('common.errors')

            <div class="panel panel-default">
                <div class="panel-heading">New task</div>
                <div class="panel-body">
                    <form action="tasks" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <label for="title">Task name:</label>
                        <input type="text" name="title" id="task-title" class="form-control" />
                        <label for="description">Description:</label>
                        <textarea rows="2" cols="50" name="description" id="task-description" class="form-control" style="resize: none;"></textarea>
                        <button type="submit">Add Task</button>
                    </form>
                </div>
            </div>



            <div class="panel panel-default">
                <div class="panel-heading">Search</div>
                <div class="panel-body">
                    <form action="tasksSearch" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        <label for="title">You can search in title and description. Minimum three (3) characters required.</label>
                        <input type="text" name="search" id="search" class="form-control" />
                        <button type="submit">Search</button>
                    </form>
                </div>
            </div>



            <div class="panel panel-default">
                <div class="panel-heading">Current Tasks</div>
                <div class="panel-body">

                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="showCompleted" id="showCompleted" value="true"
                                @if ( Cookie::get('checked')== 'on') checked="checked" @endif />Hide completed tasks
                        </label>
                    </div>



                    @foreach ( $tasks as $task )
                    <div class="task" id="task{{ $task->id }}">
                        <p name="time">{{ $task->created_at->diffForHumans() }}</p>
                        <h3 name="title">{{ $task->title }}</h3>
                        <p name="description">{{ $task->description }}</p>


                        <div id="buttonBox" style="text-align: center;">
                            <div class="inner" style="display: inline-block;">
                                <input type="button" name="edit" id="{{ $task->id }}" data-token="{{ csrf_token() }}" value="Edit" />
                            </div>

                            <div class="inner" style="display: inline-block;">
                                <input type="button" name="delete" id="{{ $task->id }}" data-token="{{ csrf_token() }}" value="Delete" />
                            </div>


                            @if ( !$task->completed )
                            <div class="inner" style="display: inline-block;">
                                <input type="button" name="complete" id="{{ $task->id }}" value="Complete" />
                            </div>
                            @endif

                        </div>

                        <hr />
                    </div>
                    @endforeach


                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {

    $(document).on("click", "input[name = 'complete']", function () {
         $.ajax({
                url: "tasksComplete",
                dataType: 'json',
                type: 'POST',
                cache: false,
                data: {
                    'id': this.id,
                    '_token': "{{ csrf_token() }}",
                },
                success: function (data) {
                     $(e.target).remove();
                }
            });
        });


    $(document).on("click", "input[name = 'delete']", function () {
         $.ajax({
                url: "tasks/" + this.id,
                dataType: 'json',
                type: 'POST',
                cache: false,
                data: {
                    'id': this.id,
                    '_method': 'delete',
                    '_token': "{{ csrf_token() }}",
                },
                success: function (data) {
                     $('#task'+ data.id).remove();
                }
            });
     });


    $('#showCompleted').change(function () {
        $.ajax({
                url: "tasksHide",
                dataType: 'html',
                type: 'POST',
                cache: false,
                data: {
                    'checked': this.checked,
                    '_token': "{{ csrf_token() }}",
                },
                success: function (data) {
                    //alert(data);
                }
        });

    });


    $(document).on("click", "input[name = 'edit']", function () {

        var time = $('#task' + (this.id)).find("p[name = 'time']").text();
        var title = $('#task' + (this.id)).find("h3[name = 'title']").text();
        var description = $('#task' + (this.id)).find("p[name = 'description']").text();
        var completed = $('#task' + (this.id)).find("input[name = 'complete']").val();

        if (completed == 'Complete') {
           completed = '';
        } else {
           completed = 'checked';
        }

        $('#task' + (this.id)).replaceWith('<div class="task" id="task' + this.id + '"><p name="time">' + time + '</p><form action="tasks" method="POST" class="form-horizontal"><label for="title">Task name:</label><input type="text" name="title" id="task-title" value="' + title + '" class="form-control" /><label for="description">Description:</label><textarea rows="2" cols="50" name="description" id="task-description" class="form-control" style="resize: none;">' + description + '</textarea> <input type="checkbox" name="completed" id="completed" ' + completed + '/> Completed <br/><input type="button" name="cancel" id="' + this.id + '" value="Cancel" /><input type="button" name="save" id="' + this.id + '" data-token="{{ csrf_token() }}" value="Save Task"/></form><hr/></div>');

    });

    $(document).on("click", "input[name = 'cancel']", function () {

        var time = $('#task' + (this.id)).find("p[name = 'time']").text();
        var title = $('#task' + (this.id)).find("input[name = 'title']").val();
        var description = $('#task' + (this.id)).find("textarea[name = 'description']").val();
        var completed = $('#task' + (this.id)).find("input[name = 'completed']").prop('checked');

        if (!completed) {
            completed = ' <div class="inner" style="display: inline-block;"><input type="button" name="complete" id="' + this.id + '" value="Complete" /></div>';
        } else {
            completed = '';
        }

        $('#task' + (this.id)).replaceWith('<div class="task" id="task' + this.id + '"><p name="time">' + time + '</p><h3 name="title">' + title + '</h3><p name="description">' + description + '</p><div id="buttonBox" style="text-align: center;"><div class="inner" style="display: inline-block;"><input type="button" name="edit" id="' + this.id + '" data-token="{{ csrf_token() }}" value="Edit" /></div> <div class="inner" style="display: inline-block;"><input type="button" name="delete" id="' + this.id + '" data-token="{{ csrf_token() }}" value="Delete" /></div>' + completed );

    });


    $(document).on("click", "input[name = 'save']", function () {

        var time = $('#task' + (this.id)).find("p[name = 'time']").text();
        var title = $('#task' + (this.id)).find("input[name = 'title']").val();
        var description = $('#task' + (this.id)).find("textarea[name = 'description']").val();
        var completed = $('#task' + (this.id)).find("input[name = 'completed']").prop('checked');

        $.ajax({
                url: "tasksEdit",
                dataType: 'html',
                type: 'POST',
                cache: false,
                data: {
                    'id': this.id,
                    'title': title,
                    'description': description,
                    'completed': completed,
                    '_token': "{{ csrf_token() }}",
                },
                success: function (data) {

                if (!completed) {
                    completed = ' <div class="inner" style="display: inline-block;"><input type="button" name="complete" id="' + data + '" value="Complete" /></div>';
                } else {
                    completed = '';
                }

                $('#task' + data ).replaceWith('<div class="task" id="task' + data + '"><p name="time">' + time + '</p><h3 name="title">' + title + '</h3><p name="description">' + description + '</p><div id="buttonBox" style="text-align: center;"><div class="inner" style="display: inline-block;"><input type="button" name="edit" id="' + data + '" data-token="{{ csrf_token() }}" value="Edit" /></div> <div class="inner" style="display: inline-block;"><input type="button" name="delete" id="' + data + '" data-token="{{ csrf_token() }}" value="Delete" /></div>' + completed );
				}
			});
    });

});
</script>
@endsection