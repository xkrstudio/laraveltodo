<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
           'title' => 'First task',
           'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi nec nibh cursus, elementum enim vel, mollis metus. Duis euismod, ipsum nec blandit gravida, diam neque imperdiet purus, convallis accumsan dolor nisi id odio.',
           'completed' => 1,
           'created_at' => '2017-08-02 18:30:00',
       ]);

        DB::table('tasks')->insert([
           'title' => 'Second task',
           'description' => 'Lorem ipsum lacinia orci arcu. Curabitur ipsum odio, tempus quis tellus sed, finibus aliquet purus. Fusce facilisis condimentum congue. Etiam cursus, est non lobortis auctor, nunc lorem convallis dui, sit amet ultricies nisi erat quis nisi. ',
           'completed' => 0,
           'created_at' => '2017-08-02 17:00:00',
        ]);
    }
}
