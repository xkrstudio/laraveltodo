<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use App\Task;

class TaskController extends Controller
{

    protected $tasks;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $value = $request->cookie('checked');

        if ( $value == 'on' ) {
            return view('tasks', [
           'tasks' => Task::where('completed', '0')->orderBy('created_at', 'desc')->orderBy('updated_at', 'asc')->get()
       ]);

        } else {
            return view('tasks', [
            'tasks' => Task::orderBy('completed', 'asc')->orderBy('created_at', 'desc')->orderBy('updated_at', 'asc')->get()
        ]);

        }

    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|max:255',
            'description' => 'string|nullable',
        ]);
        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }
        $task = new Task;
        $task->title = $request->title;
        $task->description = $request->description;
        $task->save();
        return redirect('/');
    }

    public function edit(Request $request) {
        $validator = Validator::make($request->all(), [
            'title' => 'required|min:3|max:255',
            'description' => 'string|nullable',
            'completed' => 'string|min:4|max:5',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $task = Task::find($request->id);

        if ( $request->completed == 'true' ) {
            $request->completed = 1;
        } else {
            $request->completed = 0;
        }

        $task->title = $request->title;
        $task->description = $request->description;
        $task->completed = (int)$request->completed;
        $task->save();
        return $request->id;

    }

    public function destroy($id) {

        if (  Task::findOrFail($id)->delete() ) {
            return response()->json([
            'status' => 'success',
            'id' => $id,
        ]);
        }

        return redirect('/');
    }

    public function completed(Request $request) {

        $task = Task::find($request->id);

        $task->completed = 1;

        if ( $task->save() ) {
            return response()->json([
            'status' => 'success',
        ]);
        }

        return redirect('/');
    }

    public function hide(Request $request) {
        if (  $request->checked == 'true' ) {
            return response('1')->cookie('checked', 'on', 360);
        } else {
            return response('1')->cookie('checked', 'off', 360);
        }
    }

    public function search(Request $request) {
        $validator = Validator::make($request->all(), [
           'search' => 'required|min:3|max:255',
       ]);
        if ($validator->fails()) {
            return redirect('/')
                ->withInput()
                ->withErrors($validator);
        }

        $search = $request->search;

        return view('tasks', [
          'tasks' => Task::where('title', 'like', '%'.$search.'%')->orWhere('description', 'like', '%'.$search.'%')->orderBy('updated_at', 'asc')->get()
      ]);


    }

}